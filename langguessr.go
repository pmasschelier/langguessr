package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

type Family struct {
	Code     string
	Name     string
	Families []string
}

type Lang struct {
	Family
	Count uint
}

type Sentence struct {
	Code     string
	Sentence string
}

type TranslatedSentence struct {
	Sentence
	Trads []Sentence
}

var db *sql.DB
var langs []Lang
var families []Family
var mainPage string

func main() {
	// Load environment variables from .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file:", err)
	}

	connectDB()
	var start, end time.Time

	if len(os.Args) > 1 && os.Args[1] == "init" {
		fmt.Println("Languages loading...")
		start = time.Now()
		loadLanguages()
		end = time.Now()
		fmt.Println("Languages loaded in", end.Sub(start))

		fmt.Println("Sentences loading...")
		start = time.Now()
		loadSentences()
		end = time.Now()
		fmt.Println("Sentences loaded in", end.Sub(start))

		fmt.Println("Links loading...")
		start = time.Now()
		loadLinks()
		end = time.Now()
		fmt.Println("Sentences links loaded in", end.Sub(start))

		return
	}

	rows, err := db.Query("SELECT * FROM SentencesLanguages")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var l Lang
		var families string

		rows.Scan(&l.Code, &l.Name, &l.Count, &families)
		l.Families = nil
		if len(families) != 0 {
			l.Families = strings.Split(families, ":")
		}
		langs = append(langs, l)
	}

	rows, err = db.Query("SELECT familyCode, name, families FROM LanguageFamilies")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var f Family
		var parents string

		rows.Scan(&f.Code, &f.Name, &parents)
		f.Families = nil
		if len(parents) != 0 {
			f.Families = strings.Split(parents, ":")
		}
		families = append(families, f)
	}

	rand.Seed(time.Now().UnixNano())

	content, err := os.ReadFile("index.html")
	if err != nil {
		log.Fatal("Error while reading index.html: ", err)
	}
	mainPage = string(content)

	http.HandleFunc("/", handler)
	http.HandleFunc("/random/", getRandomSentences)
	http.HandleFunc("/langs/", getLangList)
	http.Handle("/public/", http.StripPrefix("/public/", http.FileServer(http.Dir("public"))))

	fmt.Println("Launch server: http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, mainPage)
}

func sendJson(w http.ResponseWriter, obj any) {
	bytes, err := json.Marshal(obj)
	if err != nil {
		log.Fatal("Couldn't convert obj to JSON:", err)
	}
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, string(bytes))
}

func getRandomSentences(w http.ResponseWriter, r *http.Request) {
	n_str := r.URL.Query().Get("n")
	n, err := strconv.Atoi(n_str)
	if err != nil {
		n = 20
	}
	var sentences []TranslatedSentence
	for i := 0; i < n; i++ {
		code, sentence, trads := randomSentence()
		sentences = append(sentences, TranslatedSentence{Sentence{code, sentence}, trads})
	}
	sendJson(w, sentences)
}

func getLangList(w http.ResponseWriter, r *http.Request) {
	sendJson(w, struct {
		Langs    []Lang
		Families []Family
	}{langs, families})
}

func connectDB() {
	// Capture connection properties.
	cfg := mysql.Config{
		User:                 os.Getenv("DBUSER"),
		Passwd:               os.Getenv("DBPASS"),
		Net:                  "tcp",
		Addr:                 os.Getenv("DBHOST"), // "127.0.0.1:3306",
		DBName:               os.Getenv("DBNAME"),
		AllowNativePasswords: true,
	}

	// Get a database handle.
	var err error
	db, err = sql.Open("mysql", cfg.FormatDSN())
	if err != nil {
		log.Fatal(err)
	}

	pingErr := db.Ping()
	if pingErr != nil {
		log.Fatal(pingErr)
	}
	fmt.Println("Connected!")
}

func randomSentence() (string, string, []Sentence) {
	langIndex := rand.Intn(len(langs))
	sentenceIndex := rand.Intn(int(langs[langIndex].Count))

	row := db.QueryRow("SELECT id, langCode, sentence FROM Sentences WHERE langCode = ? LIMIT 1 OFFSET ?",
		langs[langIndex].Code, sentenceIndex)

	var id uint64
	var code, sentence string
	if err := row.Scan(&id, &code, &sentence); err != nil {
		log.Fatal("Error while selecting random sentence: ", err)
	}

	rows, err := db.Query("SELECT langCode, sentence FROM Sentences JOIN SentencesLinks ON SentencesLinks.sentenceA = ? AND Sentences.id = SentencesLinks.sentenceB AND Sentences.langCode IN (?, ?)",
		id, "fra", "eng")
	if err != nil {
		if err != sql.ErrNoRows {
			log.Fatal("Error while selecting traductions of: ", id, err)
		}
	}
	defer rows.Close()

	var trads []Sentence
	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var code, sentence string
		rows.Scan(&code, &sentence)
		trads = append(trads, Sentence{code, sentence})
	}
	return code, sentence, trads
}
