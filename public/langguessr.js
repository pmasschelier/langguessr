const root = document.getElementById("root");
const display = document.getElementById("sentence");
const choices = document.getElementById("choice-box");
const nextButton = document.getElementById("next-button");
const trads = document.getElementById("trads");
const tradBody = document.getElementById("trads-body");
const graph = document.getElementById("lang-graph");
sentences = [];
let pendingRequest = false, pendingAnswer = false;

let langs = {}, families = {};
window.onload = async () => {
	let res = await fetch("langs/");
	if (res.ok) {
		let langsObj = await res.json();
		langsObj.Langs.forEach(l => langs[l.Code] = { name: l.Name, families: l.Families });
		langsObj.Families.forEach(f => families[f.Code] = { name: f.Name, families: f.Families });
	}
	await fetchSentences();
	setMode(0);
	setInterval(fetchSentences, 500);
}

let mode = 0;
function setMode(newMode) {
	mode = newMode;
	switch (mode) {
		case 0:
			root.style.display = 'block';
			nextButton.style.visibility = 'hidden';
			display.innerHTML = sentences[0].Sentence;
			trads.style.display = 'none';
			tradBody.innerHTML = '';
			graph.innerHTML = '';
			updateButtons();
			break;
		case 1:
			trads.style.display = 'block';
			nextButton.style.visibility = 'visible';
			break;
	}
}

nextButton.addEventListener('click', e => {
	sentences.shift();
	setMode(0);
});

function answerClicked(e) {
	if (mode != 0) return;
	let answer = sentences[0].Code;
	let given = e.target.dataset.langcode;
	if (answer != given)
		e.target.classList.add('error');
	let buttons = document.querySelectorAll("#choice-box .button");
	buttons.forEach(b => {
		if (answer == b.dataset.langcode)
			b.classList.add('answer');
	});

	if (sentences[0].Trads) {
		for (let i = 0; i < sentences[0].Trads.length; i++) {
			let trad = sentences[0].Trads[i];
			let row = document.createElement("tr");
			row.innerHTML = `<td>${langs[trad.Code].name}</td><td>${trad.Sentence}</td>`;
			tradBody.appendChild(row);
		}
	}

	const common_families = new Set([...(langs[answer].families ?? []), ...(langs[given].families ?? [])]);
	if (common_families.size != 0) {
		const getNode = (id, label) => ({ id, label, font: { size: 20 }, margin: 20 });
		const getEdge = (from, to) => ({ from, to, arrows: 'from' });
		const lang_nodes = [], lang_edges = [];
		for (lang of common_families.keys()) {
			lang_nodes.push(getNode(lang, families[lang].name));
			if (families[lang].families.length > 1)
				lang_edges.push(getEdge(lang, families[lang].families.at(-2)));
		}
		lang_nodes.push(getNode(answer, langs[answer].name));
		if (langs[answer].families)
			lang_edges.push(getEdge(answer, langs[answer].families.at(-1)));
		if (answer != given) {
			lang_nodes.push(getNode(given, langs[given].name));
			if (langs[given].families)
				lang_edges.push(getEdge(given, langs[given].families.at(-1)));
		}
		console.log(lang_nodes);
		const nodes = new vis.DataSet(lang_nodes);
		console.log(lang_edges);
		const edges = new vis.DataSet(lang_edges);

		// create a network
		const data = { nodes, edges };
		const options = {
			layout: {
				hierarchical: {
					direction: 'UD',
				},
			},
			nodes: {
				shape: "box",
			},
			physics: {
				hierarchicalRepulsion: {
					avoidOverlap: 1,
				},
			},
		};
		new vis.Network(graph, data, options);
	}

	setMode(1);
}

function updateButtons() {
	let getInt = n => Math.floor(n * Math.random());
	let add = (code) => {
		let button = document.createElement('button');
		button.classList.add('button');
		button.dataset.langcode = code;
		button.innerHTML = langs[code].name;
		button.addEventListener('click', answerClicked);
		choices.appendChild(button);
	}

	let good = getInt(4);
	choices.innerHTML = '';
	const langCodes = Object.keys(langs)
	let goodAnswer = sentences[0].Code;
	let selected = [goodAnswer];

	for (let i = 0; i < 4; i++) {
		if (i == good) {
			add(goodAnswer);
			continue;
		}
		let randomAnswer;
		do {
			let n = getInt(langCodes.length)
			randomAnswer = langCodes[n];
		} while (randomAnswer in selected);
		selected.push(randomAnswer);
		add(randomAnswer);
	}
}

async function fetchSentences() {
	if (!pendingRequest && sentences.length <= 15) {
		pendingRequest = true;
		let res = await fetch("random/10");
		if (res.ok) {
			let el = await res.json();
			sentences = [...sentences, ...el]
		}
		pendingRequest = false;
	}
}