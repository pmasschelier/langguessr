# Interface

![Interface web](./screen.png "Interface web")

# Installation

Pour commencer à utiliser le projet vous devez le clonner et installer go.

Ensuite créez une base de donnée :

```sql
CREATE TABLE Languages (
	langCode CHAR(3) PRIMARY KEY NOT NULL,
	scope CHAR(1) NOT NULL,
	type CHAR(1) NOT NULL,
	name VARCHAR(150) NOT NULL,
	families TINYTEXT
);
```

```sql
CREATE TABLE LanguageFamilies (
	familyCode CHAR(3) PRIMARY KEY NOT NULL,
	name VARCHAR(150) NOT NULL,
	name_fr VARCHAR(150) NOT NULL,
	families TINYTEXT
);
```

```sql
CREATE TABLE Sentences (
	id INTEGER PRIMARY KEY NOT NULL,
	langCode CHAR(3) DEFAULT NULL,
	sentence MEDIUMTEXT,
	FOREIGN KEY (langCode) REFERENCES Languages(langCode)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);
```

```sql
CREATE VIEW SentencesLanguages AS SELECT Sentences.langCode, name, COUNT(*), families FROM Sentences JOIN Languages ON Languages.langCode = Sentences.langCode AND type = "L" GROUP BY langCode;
```

```sql
CREATE TABLE SentencesLinks (
	sentenceA int NOT NULL,
	sentenceB int NOT NULL,
	FOREIGN KEY (sentenceA) REFERENCES Sentences(id)
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	FOREIGN KEY (sentenceB) REFERENCES Sentences(id)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);
```

Créez ensuite un fichier .env à la racine du projet contenant
```sh
DBUSER="votre nom d'utilisateur pour la db"
DBPASS="votre password"
DBHOST="localhost"
DBNAME="le nom de la base de donnée que vous avez créé"
```

Ensuite la première fois, pour remplir la base de données lancer :
```sh
go run . init
```

Pour le lancer les fois suivantes: 

```sh
go run .
```