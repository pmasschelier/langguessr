package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func loadSentences() {
	file, err := os.Open("sentences.csv")
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	// Create a new scanner
	scanner := bufio.NewScanner(file)
	var lineNumber uint64 = 0
	ok := true

	// Iterate over lines
	for ok {

		t1 := time.Now()

		var sentences []interface{}
		const counterMax = 10000
		counter := 0
		for {
			ok = scanner.Scan()
			if !ok {
				break
			}

			line := scanner.Text()
			parts := strings.Split(line, "\t")

			if len(parts) != 3 {
				fmt.Fprintf(os.Stderr, "Following line can't be splited in three parts (id, langCode, sentence): %s\n", line)
				continue
			}
			_, err := strconv.ParseUint(parts[0], 10, 64)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Field id in the following line is not a positive integer: %s\n", line)
				continue
			}

			/* if len(parts[1]) != 3 {
				fmt.Fprintf(os.Stderr, "Language code is not ISO: %s\n", line)
				continue
			} */

			sentences = append(sentences, parts[0], parts[1], parts[2])

			lineNumber++
			counter++
			if counter >= counterMax {
				break
			}
		}

		t2 := time.Now()

		valString := strings.TrimSuffix(strings.Repeat("(?, ?, ?), ", counter), ", ")
		command := fmt.Sprintf("INSERT IGNORE INTO Sentences (id, langCode, sentence) VALUES %s", valString)
		_, err := db.Exec(command, sentences...)
		if err != nil {
			log.Fatalf("Failed to add sentence: %v", err)
		}

		t3 := time.Now()

		fmt.Printf("%d lines written (%d, %d)\r\027", lineNumber, t2.Sub(t1).Microseconds(), t3.Sub(t2).Microseconds())
	}

	fmt.Println()

	// Check for scanner errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func loadLinks() {
	file, err := os.Open("links.csv")
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	// Create a new scanner
	scanner := bufio.NewScanner(file)
	var lineNumber uint64 = 0
	ok := true

	// Iterate over lines
	for ok {

		t1 := time.Now()

		var sentences []interface{}
		const counterMax = 10000
		counter := 0

		for {
			ok = scanner.Scan()
			if !ok {
				break
			}

			line := scanner.Text()
			parts := strings.Split(line, "\t")

			if len(parts) != 2 {
				fmt.Fprintf(os.Stderr, "Following line can't be splited in three parts (sentenceA, sentenceB): %s\n", line)
				continue
			}
			_, err := strconv.ParseUint(parts[0], 10, 64)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Field langA in the following line is not a positive integer: %s\n", line)
				continue
			}
			_, err = strconv.ParseUint(parts[1], 10, 64)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Field langA in the following line is not a positive integer: %s\n", line)
				continue
			}

			sentences = append(sentences, parts[0], parts[1])

			lineNumber++
			counter++
			if counter >= counterMax {
				break
			}
		}

		t2 := time.Now()

		valString := strings.TrimSuffix(strings.Repeat("(?, ?), ", counter), ", ")
		command := fmt.Sprintf("INSERT IGNORE INTO SentencesLinks (sentenceA, sentenceB) VALUES %s", valString)
		_, err := db.Exec(command, sentences...)
		if err != nil {
			log.Fatalf("Failed to add link: %v", err)
			continue
		}

		t3 := time.Now()

		fmt.Printf("%d lines written (%d, %d)\r\027", lineNumber, t2.Sub(t1).Microseconds(), t3.Sub(t2).Microseconds())
	}

	fmt.Println()

	// Check for scanner errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func loadLanguages() {
	f_iso639_3, err := os.Open("iso-639-3_20230123.tsv")
	if err != nil {
		log.Fatal(err)
	}

	defer f_iso639_3.Close()

	// Create a new scanner
	scanner := bufio.NewScanner(f_iso639_3)
	hierarchy := getLanguagesHierarchy()

	// Iterate over lines

	var sentences []interface{}
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, "\t")

		if len(parts) != 8 {
			fmt.Fprintf(os.Stderr, "Following line can't be splited in 8 parts: %s\n", line)
			continue
		}

		if len(parts[0]) != 3 {
			fmt.Fprintf(os.Stderr, "Language code is not ISO: %s\n", line)
			continue
		}

		sentences = append(sentences, parts[0], parts[4], parts[5], parts[6], strings.Join(hierarchy[parts[0]], ":"))
	}
	valString := strings.TrimSuffix(strings.Repeat("(?, ?, ?, ?, ?), ", len(sentences)/5), ", ")
	command := fmt.Sprintf("INSERT INTO Languages (langCode, scope, type, name, families) VALUES %s", valString)
	_, err = db.Exec(command, sentences...)
	if err != nil {
		log.Fatalf("Failed to add language: %v", err)
	}

	// Check for scanner errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func getLanguagesHierarchy() map[string][]string {
	f_iso639_5_to_iso639_3, err := os.Open("iso639-5_to_iso639-3.tsv")
	if err != nil {
		log.Fatal(err)
	}

	defer f_iso639_5_to_iso639_3.Close()

	// Create a new scanner
	scanner := bufio.NewScanner(f_iso639_5_to_iso639_3)

	hierarchy := make(map[string][]string)

	// Iterate over lines
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, "\t")

		if len(parts) != 3 {
			continue
		}

		if len(parts[0]) != 3 {
			fmt.Fprintf(os.Stderr, "Language code is not ISO: %s\n", line)
			continue
		}

		langs := strings.Split(parts[2], " ")

		for _, l := range langs {
			if len(l) != 3 {
				continue
			}
			hierarchy[l] = append(hierarchy[l], parts[0])
		}
	}

	f_iso639_5, err := os.Open("iso639-5-table.tsv")
	if err != nil {
		log.Fatal(err)
	}

	defer f_iso639_5.Close()

	// Create a new scanner
	scanner = bufio.NewScanner(f_iso639_5)
	famhierarchy := make(map[string][]string)

	// Iterate over lines
	var sentences []interface{}
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, "\t")

		if len(parts) != 4 {
			fmt.Fprintf(os.Stderr, "Following line can't be splited in 4 parts: %s\n", line)
			continue
		}

		if len(parts[0]) != 3 {
			fmt.Fprintf(os.Stderr, "Language code is not ISO: %s\n", line)
			continue
		}

		families := strings.Split(parts[3], " : ")
		famhierarchy[parts[0]] = families
		sentences = append(sentences, parts[0], parts[1], parts[2], strings.Join(families, ":"))
	}
	valString := strings.TrimSuffix(strings.Repeat("(?, ?, ?, ?), ", len(sentences)/4), ", ")
	command := fmt.Sprintf("INSERT INTO LanguageFamilies (familyCode, name, name_fr, families) VALUES %s", valString)
	_, err = db.Exec(command, sentences...)
	if err != nil {
		log.Fatalf("Failed to add language family: %v", err)
	}

	for lang, value := range hierarchy {
		var fam_max string
		maxDeep := 0
		for _, family := range value {
			deep := len(famhierarchy[family])
			if deep > maxDeep {
				fam_max = family
				maxDeep = deep
			}
		}
		hierarchy[lang] = famhierarchy[fam_max]
	}
	return hierarchy
}
